/*
 2019/01/04
 By Mej, for Impact Photonics

 Interface_PCB_v2 with ATMega32U4 bootloaded as Arduino Leonardo
*/


//Libs
/////////////////////////////////////////////////////////////////////

#include <Wire.h>
#include <PID_v1.h> //https://playground.arduino.cc/Code/PIDLibrary


//MCU pin-maping
/////////////////////////////////////////////////////////////////////

const byte LED_B = 8;  //pin28 PB4-ADC11         Led Blue (bottom of MCU)
const byte LED_O = A5;  //pin41 PF0-ADC0         Led Orange (right of MCU)

const byte CS_1 = A4;  //pin40  PF1
const byte CS_2 = A3;  //pin39  PF4-TCK-ADC4
const byte En_V3 = A2; //pin38  PF5

const byte PWM_1  = 5;  //pin31* PC6-OC3A
const byte SPDT_1 = 13; //pin32* PC7-OC4A
const byte PWM_2  = 9;  //pin29* PB5-OC1A-ADC12
const byte SPDT_2 = 10; //pin30* PB3-OC1B-ADC13
const byte PWM_3  = 6;  //pin27* PD7-OC4D-ADC10-T0
const byte FAN_1  = 11; //pin12* PB7-OC0A

const byte TRIG_1 = 1; //pin21 PD3-TX         Trig to RasPI
const byte TRIG_2 = 4; //pin25 PD4-ICP1-ADC8  Trig from RasPI
const byte SW = 12;     //pin26  PD6-ADC9      COM of dual SPDT (U-_1 U-_2) -> U- and (U+_1 U+_2) -> U+
const byte INT_1 = 1;  //pin20 PD2-RX            /INT of TCA9534 (left side)
const byte LDAC_1 = 7; //pin1  PE6-AIN0-Int6     Latch of MCP4728

//const byte A1 = A1; //pin37  PF6-ADC6
//const byte A0 = A0; //pin36  PF7-ADC7

//pin values

byte pwm_1 = 0;
byte spdt_1 = 0;
byte pwm_2 = 0;
byte spdt_2 = 0;
byte pwm_3 = 0;
byte fan_1 = 0;

bool trig_2 = 0;
bool sw = 0;
bool int_1 = 0;
bool ldac_1 = 0;

int adc_0 = 0;
int adc_1 = 0;
int max_0 = 0;
int max_1 = 0;
int min_0 = 0;
int min_1 = 0;


// GPIO expender values
//U5: TCA9534 left
byte Led_on_1 = 0; //A0-A1-A2 of CD74HC237 (left Driver_IMS) 
bool En_1 = 0;     //Enable of TLV4110 (left Driver_IMS)
byte RasPI = 0;    //P4-P5-P6-P7 Reading from RasPI connector =>Which led you want (from 0 to 7 for 1st channel, from 8 to 15 fo 2nd channel)

//U4: TCA9534 right
byte Led_on_2 = 0; //A0-A1-A2 of CD74HC237 (right Driver_IMS) 
bool En_2 = 0;     //Enable of TLV4110 (right Driver_IMS)
bool fan_2 = 0;
byte io_6 = 0;   
byte io_7 = 0;


//timing monitoring
/////////////////////////////////////////////////////////////////////
unsigned long t_0 = millis(); // gives time since startup in ms. Overflow after 50 days.
unsigned long t = millis();
unsigned long loop_cnt = 0; //loop count
const int D = 100; //loop target duration in ms

unsigned long t_led = micros(); // gives time since startup in us. Overflow after 1 hour.
//const unsigned long T_LED = 1000; //target led on in us (whith switch on time included)

//Wire and I²C functions
/////////////////////////////////////////////////////////////////////

byte data, data0, data1, data2; // used to buffer I²C data (in and/or out)

void set_I2C_register(byte ADDRESS, byte REGISTER, byte VALUE)
{
  Wire.beginTransmission(ADDRESS);
  delayMicroseconds(1);
  Wire.write(REGISTER);
  Wire.write(VALUE);
  Wire.endTransmission();
}

byte get_I2C_register(byte ADDRESS, byte REGISTER)
{
  Wire.beginTransmission(ADDRESS);
  delayMicroseconds(1);
  Wire.write(REGISTER);
  Wire.endTransmission();
  Wire.requestFrom(ADDRESS, 1); //read 1 byte
  byte x = Wire.read();
  return x;
}


//## MCP9904 (I²C thermometer, 1+3 channels)
//Address
const byte MCP9904_0 = B0011100; // 1C  R=22k  LED_IMS
const byte MCP9904_1 = B1011100; // 5C  R=6.8k Interface_PCB, left side
const byte MCP9904_2 = B1111100; // 7C  R=4.7k Interface_PCB, right side
const byte MCP9904_3 = B0111100; // 3C  R=None µThermo IMS (cuvette holder)
const byte MCP9904s[] = {MCP9904_0, MCP9904_1, MCP9904_2, MCP9904_3};
const byte MCP9904_cnt =4;
byte MCP9904 = MCP9904s[0];
//reg values
const byte MCP9904_config = B00100000; //LED Alert ON - Device active - Comparator mode Alert -  Resistance Error Correction enabled - range 0 to 128°C - Averaging enabled - AntiParallel diode enabled
const int MCP9904_high_limit=128; //signed int between 0 and 128
const int MCP9904_low_limit =0;
//store the temperature of respectivelly: Internal, Q1, Q2, Q3
float Ts[MCP9904_cnt][4] = { {66.6, 66.6, 66.6, 66.6}, //Used to store temps in [°C]
                             {66.6, 66.6, 66.6, 66.6},
                             {66.6, 66.6, 66.6, 66.6},
                             {66.6, 66.6, 66.6, 66.6} };


//## MCP4728 (Quad DAC 12bit - Vref=2.048V)
//Address
const byte MCP4728 = B1100011; //0x63  ->Udrive
const byte MCP4728_P = B1100000; //0x60  ->Peltier
//mode
const byte MCP4728_MODE = B10000000;  // Vref interne - PD=PD0=G=0
const byte MCP4728_FAST = B00000000; //Write one DAC imputs registers from Channel A to Channel D, see fig 5-7 p38
//Commands (see from p31 of datasheet)
const byte MCP4728_RESET = 0x06;
const byte MCP4728_WAKEUP = 0x09;
const byte MCP4728_UPDATE = 0x08;
const byte MCP4728_ADDRESS = 0x0C; //can be changed in EEPROM!
const byte MCP4728_HARDW = B01010000; //Write all DAC imputs registers & EEPROM from Channel A to Channel D, takes ~25ms, see figure 5-9 p40

//DAC value entre 0 et 4095 (12bit) où 1LSB = 2.048/2**16=0.5mV       2048=1024mV    512= 256mV
//R sense = 2 Ohm, I_max=350mA => Ud_max=700mV => DAC_max=1400 (effective DAC resolution = ln2(1400) = 10.5bit )
//unsigned int DACS[] = {1400, 1400, 14, 14}; // {1_low, 2_high, 2_low, 1_high} Udrive
unsigned int DACS[] = {0, 1400, 0, 1400}; // {1_low, 2_high, 2_low, 1_high} Udrive
//unsigned int DACS[] = {10, 10, 10, 10}; // {1_low, 2_high, 2_low, 1_high} Udrive
//Peltier
unsigned int DAC_ref = 3000;
int DAC_P_led = 0;
int DAC_P_cuv = 0;
//led                     0    1    2    3    4    5    6    7    
//unsigned int DAC_led[] = {800, 800, 800, 800, 800, 800, 800, 800};  //I_high target vs led
unsigned int DAC_led[] = {100, 100, 100, 100, 100, 100, 100, 100};  //I_high target vs led

//## TCA9534 (I²C GPIO expender, 8bit)
//Address
const byte TCA9534_0 =  B0100000; //  0x20 left-side (RasPI connector)
const byte TCA9534_1 =  B0100001; //  0x21 right-side (Fan&GPIO connector)
//config (1=>input, 0=>output, P7 to P0)
const byte TCA_config_0 = B11110000; //IO7-6-5-4-En_1-A2-1-0
const byte TCA_config_1 = B00000000; //IO7-6-fan2-x-En_2-A2-1-0
//default value output port
const byte PCA_default =  B00000000;
//Used to read PCA ports
byte TCA_0;
byte TCA_1;

//## PID
  //Define Variables we'll be connecting to
double Setpoint_LED, Input_LED, Output_LED; //LED PCB, petit peltier connected to Peltier_IMS channel 2
double Setpoint_CUV, Input_CUV, Output_CUV; //CUV Holder, petit peltier connected to Peltier_IMS channel 1
   //Specify the links and initial tuning parameters
   // for tunningn cf: https://en.wikipedia.org/wiki/Ziegle-Nichols_method
PID myPID_LED(&Input_LED, &Output_LED, &Setpoint_LED, 10, 2, 2, DIRECT); //
//PID myPID_CUV(&Input_CUV, &Output_CUV, &Setpoint_CUV, 20, 0.4, 0.4, REVERSE); // Ku entre 100 et 200. @150, Tu=30s => Kp=90, Ki=15, Kd=3.75;
PID myPID_CUV(&Input_CUV, &Output_CUV, &Setpoint_CUV, 100, 2, 1, DIRECT); //PID(&Input, &Output, &Setpoint, Kp, Ki, Kd, Direction) 
  //Target temps
float T_LED_target = 20;
float T_CUV_target = 30;


/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
//  SETUP
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

void setup() {

  //MCU pin set up
  /////////////////////////////////////////////////////////////////////

  pinMode(LED_B,OUTPUT);
  pinMode(LED_O,OUTPUT);
  pinMode(CS_1, OUTPUT);
  pinMode(CS_2, OUTPUT);
  pinMode(En_V3,OUTPUT);
  
  pinMode(PWM_1, OUTPUT);
  pinMode(SPDT_1,OUTPUT);
  pinMode(PWM_2, OUTPUT);
  pinMode(SPDT_2,OUTPUT);
  pinMode(PWM_3, OUTPUT);
  pinMode(FAN_1, OUTPUT);
  
  pinMode(TRIG_1,OUTPUT);
  pinMode(TRIG_2,INPUT);
  pinMode(SW,    OUTPUT);
  pinMode(INT_1, INPUT);
  pinMode(LDAC_1, OUTPUT);

  pinMode(A1,INPUT);
  pinMode(A0,INPUT);
  
  
  //Write pin idle
  
  digitalWrite(LED_B, LOW);
  digitalWrite(LED_O, LOW);
  digitalWrite(CS_1,  LOW);
  digitalWrite(CS_2,  LOW);
  digitalWrite(En_V3, HIGH); //turn LOW if other device is providing +3.3V (for I2C coms)

  digitalWrite(TRIG_1, LOW);
  digitalWrite(SW, sw);
  digitalWrite(LDAC_1, ldac_1);

  analogWrite(PWM_1, pwm_1); 
  analogWrite(SPDT_1, spdt_1); 
  analogWrite(PWM_2, pwm_2);
  analogWrite(SPDT_2, spdt_2);
  analogWrite(PWM_3, pwm_3);
  analogWrite(FAN_1, fan_1);

  trig_2 = digitalRead(TRIG_2);
  int_1 = digitalRead(INT_1);


  //MCU blink for 1s
  for(int i=0;i<10;i++)
  {
    digitalWrite(LED_B, HIGH);
    digitalWrite(LED_O, LOW);
    delay(50);
    digitalWrite(LED_B, LOW);
    digitalWrite(LED_O, HIGH);
    delay(50);
  }
  digitalWrite(LED_B, LOW);
  digitalWrite(LED_O, LOW);


  //Serial coms set-up
  Serial.begin(115200); // in BAUD = 14.4kB/s
  delay(200);
  //while (!Serial);             // wait for serial monitor to be monitoring ^^
  Serial.println("\n\n\n############################################################");
  Serial.println("#!  ATMega32U4 bootlaoded as Arduino Leonardo is awake     #");
  Serial.println("############################################################");
  Serial.println("\nSerial is up at 115200 baud");
  delay(200);

  
  //MCU PWM set up on highest frequency (62.5kHz <=> no prescaling), mode "fast pwm 8 bit" (ie mode 5, see p133/438 of datasheet)
  //notes on timer regs: https://www.locoduino.org/spip.php?article89
  Serial.println("\nTimers set for 'fast pwm'");
  byte Timer_mode = 5; // "fast pwm" (ie single slope) , 8 bit
  bool WGM_0 = bitRead(Timer_mode,0);
  bool WGM_1 = bitRead(Timer_mode,1);
  bool WGM_2 = bitRead(Timer_mode,2);
  bool WGM_3 = bitRead(Timer_mode,3);
  byte f_0 = B001; // No prescaling
  byte f_8 = B010; // clock /8
  byte f_64 = B011; // clock /64
  byte f_256 = B100; // clock /256
  byte f_1024 = B101; // clock /1024
  byte f = f_64;
  
  //PWM_2 on pin29* PB5 - OC1A - Timer 1 (reg TCCR1X) compare register OCR1A
  //SPDT_2 on pin30* PB3 - OC1B - Timer 1 (reg TCCR1X) compare register OCR1B
  TCCR1A = TCCR1A/4*4 + (WGM_1*2+WGM_0);
  TCCR1B = TCCR1A/32*32 + (WGM_3*16+WGM_2*8+f);
  Serial.println("\tTIMER 1(PWM_2, SPDT_2) set at 62kHz/8");
  
  //PWM_1 on pin31* PC6-OC3A - Timer 3 (reg TCCR3X) compare register OCR3A
  TCCR3A = TCCR3A/4*4 + (WGM_1*2+WGM_0);
  TCCR3B = TCCR3A/32*32 + (WGM_3*16+WGM_2*8+f);
  Serial.println("\tTIMER 3(PWM_1) set at 62kHz/8");
  
  //SPDT_1 on pin32*
  //PWM_3 on pin27* PD7-OC4D - Timer 4 (reg TCCR4X) compare register OCR4D   !NB! modify PLL register "PLLFRQ" for even faster PWM
  f_0 = B0001; // No prescaling
  f_8 = B0100; // clock /8
  f_64 = B0111; // clock /64
  f_256 = B1001; // clock /256
  f_1024 = B1011; // clock /1024
  f = f_64;
  
  TCCR4B = TCCR4B/16*16 + f;
  TCCR4B = TCCR4B|B10000000;
  TCCR4D = TCCR4D/4*4 + B00;
  Serial.println("\tTIMER 4(SPDT_1, PWM_3) set at 62kHz/8");

  //Changing freq of Timer0 <=> FAN_1 (pin11 would change the I2C frequency?..)
  Serial.println("\tTIMER 0(FAN_1) un-alterded");


  //Set and scan I²C bus  // Copy/Pasted/Tweeked from https://playground.arduino.cc/Main/I2cScanner
  Wire.begin();
  Wire.setClock(400000); //400kHz I²C 
  Serial.println("\nI2C Scanning...");
  byte error, address;
  int nDevices =0;
  for(address = 1; address < 128; address++ )
  {
    delay(10);
    Wire.beginTransmission(address);
    error = Wire.endTransmission();
    if (error == 0)
    {
      Serial.print("I2C device found at address 0x");
      if (address<16)
        Serial.print("0");
      Serial.print(address,HEX);
      Serial.println("  !");
 
      nDevices++;
    }
    else if (error==4)
    {
      Serial.print("Unknown error at address 0x");
      if (address<16)
        Serial.print("0");
      Serial.println(address,HEX);
    }    
  }
  if (nDevices == 0)
    Serial.println("No I2C devices found\n");
  else
    Serial.print("done\n");
  delay(100);


//Set TCA9534
  set_I2C_register(TCA9534_0, 0x03, TCA_config_0); //Config register
  set_I2C_register(TCA9534_1, 0x03, TCA_config_1);
  set_I2C_register(TCA9534_0, 0x01, PCA_default); //output port
  set_I2C_register(TCA9534_1, 0x01, PCA_default);
  //Read and print pin states
  TCA_0 = get_I2C_register(TCA9534_0, 0x00);
  TCA_1 = get_I2C_register(TCA9534_1, 0x00);
  Serial.println("\nTCA9534 are set-up");
  Serial.print("\tleft=\t");
  Serial.println(TCA_0,BIN);
  Serial.print("\tright=\t");
  Serial.println(TCA_1,BIN);


//Set MCP9904s, see datasheet p19/51 for register description
  for(int i=0; i<MCP9904_cnt; i++)
  {
    MCP9904 = MCP9904s[i];
    //temperature alert: HI LIMIT TEMP HI BYTE :REGISTER 5-10
    set_I2C_register(MCP9904, 0x07, MCP9904_high_limit); //int
    set_I2C_register(MCP9904, 0x0D, MCP9904_high_limit); //ext1 
    set_I2C_register(MCP9904, 0x15, MCP9904_high_limit); //ext2
    set_I2C_register(MCP9904, 0x2C, MCP9904_high_limit); //ext3
    //temperature alert: LO LIMIT TEMP HI BYTE :REGISTER 5-12
    set_I2C_register(MCP9904, 0x08, MCP9904_low_limit); 
    set_I2C_register(MCP9904, 0x0E, MCP9904_low_limit);
    set_I2C_register(MCP9904, 0x16, MCP9904_low_limit); 
    set_I2C_register(MCP9904, 0x2D, MCP9904_low_limit); 
    //configuration
    set_I2C_register(MCP9904, 0x03, MCP9904_config); 
    set_I2C_register(MCP9904, 0x09, MCP9904_config); //Config register also an 0x09 register?..
  }
  Serial.println("\nMCP9904s are set-up");


  //Set MCP4728
  delay(1);
  Serial.println(" ");
  Serial.print("MCP4728 is RESET, ");
  Wire.beginTransmission(MCP4728);
  Wire.write(MCP4728_RESET);
  Wire.endTransmission();
  delay(100);
  Serial.print("loaded with [");
  Wire.beginTransmission(MCP4728);
  Wire.write(MCP4728_HARDW);
  for(int i=0; i<4; i++){
    Serial.print(DACS[i]/2/2); //0.5mV per value, 2 Ohm sensing
    Serial.print(" ");
    byte Byte1 = (DACS[i]>>8); //first bits (12-8=4bits)
    Byte1 = MCP4728_MODE|Byte1;
    byte Byte2 = DACS[i]&0xff; //last byte
    Wire.write(Byte1);
    Wire.write(Byte2);
    /*
    Serial.print("=");
    Serial.print(Byte1,BIN);
    Serial.print(" ");
    Serial.print(Byte2,BIN);
    */
    }
  Wire.endTransmission();
  delay(100);
  Serial.println("] mA and is ready to go");


  //Set MCP4728_P
  delay(1);
  Serial.println(" ");
  Serial.print("MCP4728_P is RESET, ");
  Wire.beginTransmission(MCP4728_P);
  Wire.write(MCP4728_RESET);
  Wire.endTransmission();
  delay(100);
  Serial.print("loaded with [");
  Wire.beginTransmission(MCP4728_P);
  Wire.write(MCP4728_HARDW);
  for(int i=0; i<4; i++){
    Serial.print(DAC_ref/2); //0.5mV per value, 3000=>1500mV=1.5V=Vref MAX1968
    Serial.print(" ");
    byte Byte1 = (DAC_ref>>8); //first bits (12-8=4bits)
    Byte1 = MCP4728_MODE|Byte1;
    byte Byte2 = DAC_ref&0xff; //last byte
    Wire.write(Byte1);
    Wire.write(Byte2);
    }
  Wire.endTransmission();
  delay(100);
  Serial.println("] mV and is ready to go");



//Set PIDs

  Input_LED = Ts[0][0]; //initialize the variables we're linked to
  Setpoint_LED = T_LED_target;
  Output_LED = 128;
  myPID_LED.SetMode(AUTOMATIC); //turn the PID on
  myPID_LED.SetSampleTime(200);
  Serial.println("\nLED_IMS PID is set-up");
  
  Input_CUV = Ts[3][0]; //initialize the variables we're linked to
  Setpoint_CUV = T_CUV_target;
  Output_CUV = 128;
  myPID_CUV.SetMode(AUTOMATIC); //turn the PID on
  myPID_CUV.SetSampleTime(200);
  Serial.println("\nCUVETTE PID is set-up");

  
  Serial.println("\n\n");
}



/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
// LOOP
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

void loop() {

//Start of usefull loop
  digitalWrite(TRIG_1,HIGH);
  loop_cnt = loop_cnt+1;
  t_0 = millis();
  //LED Orange is ON alone
  digitalWrite(LED_B, LOW);
  digitalWrite(LED_O, HIGH);


//// Read input pin
  trig_2 = digitalRead(TRIG_2);
  int_1 = digitalRead(INT_1);

  
//// Read TCA
  TCA_0 = get_I2C_register(TCA9534_0, 0x00); //Output port 0
  RasPI = TCA_0/16; //code for the LED
  TCA_1 = get_I2C_register(TCA9534_1, 0x00); //Output port 1

//!!! FAKE led target generation
  trig_2 = 1;
  //RasPI = 12; //950nm
  //RasPI = 11; //850nm
  RasPI = 10; //Rouge
  //RasPI = 7; //Orange


// Read MCP99004s
  for(int i=0; i<MCP9904_cnt; i++)
  {  
    MCP9904 = MCP9904s[i]; //select address
    
    data1 = get_I2C_register(MCP9904, 0x00); //T_int high byte, signed int
    data2 = get_I2C_register(MCP9904, 0x29); //T_int low byte, 3 usefull bits only (MSB), steps of 0.125°C
    Ts[i][0] = data1 + data2/32*0.125;
    
    data1 = get_I2C_register(MCP9904, 0x01); //T_1 high byte
    data2 = get_I2C_register(MCP9904, 0x10); //T_1 low byte
    Ts[i][1] = data1 + data2/32*0.125;

    data1 = get_I2C_register(MCP9904, 0x23); //T_2 high byte
    data2 = get_I2C_register(MCP9904, 0x24); //T_2 low byte
    Ts[i][2] = data1 + data2/32*0.125;

    data1 = get_I2C_register(MCP9904, 0x2A); //T_2 high byte
    data2 = get_I2C_register(MCP9904, 0x2B); //T_2 low byte
    Ts[i][3] = data1 + data2/32*0.125;
  }


//update pins & DACS value

  pwm_3 = 0;
  fan_1 = 255;
  spdt_1=128; //U_drive_high
  spdt_2=128;
  
  io_7 = 1; //En2 of Peltier_IMS
  io_6 = 1; //En1 of Peltier_IMS
  fan_2 = 0;

  if(RasPI<8){      //if led of channel 1
      En_1 = 1;
      pwm_1 = 255;
      En_2 = 0;
      pwm_2 = 0;
      DACS[0]=DAC_led[RasPI]; //1_high
      Led_on_1 = RasPI;
      Led_on_2 = 0;
      sw=0;}
  if(RasPI>=8){      //if led of channel 2
      En_1 = 0;
      pwm_1 = 0;
      En_2 = 1;
      pwm_2 = 255;
      DACS[1]=DAC_led[RasPI-8]; //2_high
      Led_on_1 = 0;
      Led_on_2 = RasPI;
      sw=1;}
  if(trig_2==0){      //if ligh-source disabled
      En_1 = 0;
      pwm_1 = 0;
      En_2 = 0;
      pwm_2 = 0;}

////writte MCP4728
//  Wire.beginTransmission(MCP4728);
//  for(int i=0; i<=3; i++){
//    byte Byte1 = MCP4728_FAST|(DACS[i]>>8); //first bits (12-8=4bits) + config
//    Wire.write(Byte1);
//    byte Byte2 = DACS[i]&0xff; //last byte
//    Wire.write(Byte2);
//    }
//  Wire.endTransmission();
//  //delay(1);
//  digitalWrite(LDAC_1, 0);
//  delayMicroseconds(10);
//  digitalWrite(LDAC_1, 1); 
  
//Write MCU pins
  analogWrite(PWM_1, pwm_1);   //Line decoder at zero
  analogWrite(PWM_2, pwm_2); 
  analogWrite(PWM_3, pwm_3);   //output
  analogWrite(FAN_1, fan_1);   //output
  analogWrite(SPDT_1, spdt_1); //Udrive switch
  analogWrite(SPDT_2, spdt_2); 
  digitalWrite(SW, sw);        //ADC switch

//Write GPIO expender pins 
  set_I2C_register(TCA9534_0, 0x01, En_1*8 + Led_on_1%8); // 4 last bits: En_1-A2-A1-A0
  set_I2C_register(TCA9534_1, 0x01, io_7*128 + io_6*64 + fan_2*32 + En_2*8 + Led_on_2%8); //IO7-6-fan2-x-En_2-A2-1-0

  
// PID Algorithme

  //CUV PID
  float phi = 2.0*3.1416*0.2*(t/1000.0/60); //2.pi.f.t     frequency 1/5min= 0.2/min
  if(cos(phi)>0){T_CUV_target=40;}
  if(cos(phi)<=0){T_CUV_target=10;}
 
  Input_CUV = Ts[3][0];
  Setpoint_CUV = T_CUV_target;
  myPID_CUV.Compute();
  float z = (Output_CUV-128)/128.0*800; //! Output varies between 0 and 255 !, DAC varies between +/-1000 arounf DAC_ref
  DAC_P_cuv = floor(z);
  //DAC_P_cuv = floor( 800*cos(phi) ); //cosinus between -800 and +800 (Amp=800, Offset=0)
  

  //LED PID
  Input_LED = Ts[0][0];
  Setpoint_LED = T_LED_target;
  myPID_LED.Compute();
  float zz = (Output_LED-128)/128.0*1000; //! Output varies between 0 and 255 !, DAC varies between +/-1000 arounf DAC_ref
  DAC_P_led = floor(zz);
  

  

  //writte MCP4728_P 
  if(loop_cnt%1==0) //every loop
  {
    Wire.beginTransmission(MCP4728_P);

    unsigned int value = DAC_ref + DAC_P_cuv; //1st DAC (Peltie 1->  Cuv) on channel 1
    byte Byte1 = MCP4728_FAST|(value>>8); //first bits (12-8=4bits) + config
    byte Byte2 = value&0xff; //last byte
    Wire.write(Byte1);
    Wire.write(Byte2);

    for(int ch=1;ch<=3;ch++){
      unsigned int value = DAC_ref + DAC_P_led; //Other DAC (Peltie2->LED) on channels 2, 3 &4
      byte Byte2 = value&0xff; //last byte
      byte Byte1 = MCP4728_FAST|(value>>8); 
      Wire.write(Byte1);
      Wire.write(Byte2);
      }

    Wire.endTransmission();
    //delay(10);
  }



//// LED LOOP
//  delay(1);
//  digitalWrite(TRIG_1,HIGH);
//  delayMicroseconds(10);
//  digitalWrite(TRIG_1,LOW);
//  t_led = micros(); // time reference
//  
//  //1st driver
//  En_1 = 1;
//  set_I2C_register(TCA9534_0, 0x01, En_1*8);
//  
//  
//  while( (micros()-t_led) < T_LED ){true;} //Wait T_LED before starting 1st loop
//  for(byte led=0;led<8;led++)
//  {
//    Led_on_1 = 7-led;
//    set_I2C_register(TCA9534_0, 0x01, En_1*8 + Led_on_1%8);
//    
//    analogWrite(PWM_1, 255);
//    while( (micros()-t_led) < T_LED*(led+2) ){true;}
//    analogWrite(PWM_1, 0);
//  }
//  En_1 = 0;
//  set_I2C_register(TCA9534_0, 0x01, En_1*8);
//
//  //2nd driver
//  En_2 = 1;
//  set_I2C_register(TCA9534_1, 0x01, io_7*128 + io_6*64 + fan_2*32 + En_2*8);
//  digitalWrite(SW, HIGH);
//
//  while( (micros()-t_led) < T_LED*10 ){true;} //Wait T_LED*10 before starting 2nd loop
//  for(byte led=0;led<8;led++)
//  {
//    Led_on_2 = led;
//    set_I2C_register(TCA9534_1, 0x01, io_7*128 + io_6*64 + fan_2*32 + En_2*8 + Led_on_2%8);
//    
//    analogWrite(PWM_2, 255);
//    while( (micros()-t_led) < T_LED*(led+11) ){true;}
//    analogWrite(PWM_2, 0);
//  }
//  En_2 = 0;
//  set_I2C_register(TCA9534_1, 0x01, io_7*128 + io_6*64 + fan_2*32 + En_2*8);


// ADC measurement with average and max/min over R=16 samples 
  digitalWrite(TRIG_1,LOW);
  int R=8; //int are 2 bytres (16 bit), coding from -32,768 to 32,767 => Rmax=32 before saturation
  adc_0 = 0;
  min_0 = 1024; 
  max_0 = 0;
  adc_1 = 0;
  min_1 = 1024; 
  max_1 = 0;
  for(int r=0; r<16; r++){
    int adc=analogRead(A0);
    adc_0 += adc;
    if(adc<min_0){min_0=adc;}
    if(adc>max_0){max_0=adc;}
    adc=analogRead(A1);
    adc_1 += adc;
    if(adc<min_1){min_1=adc;}
    if(adc>max_1){max_1=adc;}
    }
  float moy_0 = adc_0/R;
  float moy_1 = adc_1/R;

  
//End of usefull loop
  t = millis();
  //LED Blue is ON alone
  digitalWrite(LED_B, HIGH);
  digitalWrite(LED_O, LOW);  
 
//Serial Print (minimal)
  if(loop_cnt%1==0)
  {
    String sep="\t";
    String header="#\n#Loop"+sep+"t[s]"+sep+"led"+sep+
    "I[mA]"+sep+sep+"U[mV]"+sep+sep+
    "P_led"+sep+"P_cuv"+sep+
    "T_LED"+sep+sep+sep+sep+"T_LEFT"+sep+sep+sep+sep+"T_RIGHT"+sep+sep+sep+sep+"T_CUV"+sep+sep+sep+sep;
    if(loop_cnt<4){Serial.println(header);}
    //timing
    Serial.print(loop_cnt,DEC);
    Serial.print(sep);
    Serial.print(t/1000.0, 3);
  //  //RasPI
  //  Serial.print("\t; RasPI= ");
  //  Serial.print(bitRead(RasPI,3));
  //  Serial.print(bitRead(RasPI,2));
  //  Serial.print(bitRead(RasPI,1));
  //  Serial.print(bitRead(RasPI,0));
  //  Serial.print("-");
  //  Serial.print(trig_2);
    //led
    Serial.print(sep);
    if(trig_2==1){Serial.print(RasPI, DEC);}
    if(trig_2==0){Serial.print("X");}
    //led state
    Serial.print(sep);
    Serial.print(moy_0*4.9/2,0); //5 volts / 1024 units = 4.9mV/unit R=2 Ohm
    Serial.print(sep);
    Serial.print( max( 4.9/2, (max_0-min_0)/2 ),0 );
    Serial.print(sep);
    Serial.print((moy_1-moy_0)*4.9,0);
    Serial.print(sep);
    Serial.print( max(4.9/2,max_1-min_1),0 );
    //DAC Peltier
    Serial.print(sep);
    Serial.print(DAC_P_led);
    Serial.print(sep);
    Serial.print(DAC_P_cuv);
    //temperature LED
    Serial.print(sep);
    Serial.print(Ts[0][0],1);
    Serial.print(sep);
    Serial.print(Ts[0][1],1);
    Serial.print(sep);
    Serial.print(Ts[0][2],1);
    Serial.print(sep);
    Serial.print(Ts[0][3],1);
    //temperature LEFT
    Serial.print(sep);
    Serial.print(Ts[1][0],1);
    Serial.print(sep);
    Serial.print(Ts[1][1],1);
    Serial.print(sep);
    Serial.print(Ts[1][2],1);
    Serial.print(sep);
    Serial.print(Ts[1][3],1);
    //temperature RIGHT
    Serial.print(sep);
    Serial.print(Ts[2][0],1);
    Serial.print(sep);
    Serial.print(Ts[2][1],1);
    Serial.print(sep);
    Serial.print(Ts[2][2],1);
    Serial.print(sep);
    Serial.print(Ts[2][3],1);
    //temperature CUV
    Serial.print(sep);
    Serial.print(Ts[3][0],1);
    Serial.print(sep);
    Serial.print(Ts[3][1],1);
    Serial.print(sep);
    Serial.print(Ts[3][2],1);
    Serial.print(sep);
    Serial.println(Ts[3][3],1);

  }
  
  

////Serial Print (full data)
////    if(loop_cnt%10==0)
//  if(loop_cnt<0)
//  {
//   //input pin status
//    Serial.print("\nRasPI= ");
//    Serial.print(bitRead(RasPI,3));
//    Serial.print(bitRead(RasPI,2));
//    Serial.print(bitRead(RasPI,1));
//    Serial.print(bitRead(RasPI,0));
//    Serial.print(" = ");
//    Serial.println(RasPI, DEC);
//
//    Serial.print("Trig_2= ");
//    Serial.println(trig_2);
//    
//    Serial.println("\nA0\tA1");
//    Serial.print(adc_0,DEC);
//    Serial.print("\t");
//    Serial.println(adc_1,DEC);
//
//    //output pin status
//    Serial.println("\nDriver\tEn\tLed_on");
//    Serial.print("left\t");
//    Serial.print(En_1);
//    Serial.print("\t");
//    Serial.println(Led_on_1,DEC);
//    Serial.print("right\t");
//    Serial.print(En_2);
//    Serial.print("\t");
//    Serial.println(Led_on_2,DEC);
//
//    Serial.print("\nsw=");
//    Serial.println(sw);
//
////    //PWMs status
////    Serial.println("\nPWM_1 \t SPDT_1");
////    Serial.print(pwm_1);
////    Serial.print("\t ");
////    Serial.println(spdt_1);
////
////    Serial.println("\nPWM_2 \t SPDT_2");
////    Serial.print(pwm_2);
////    Serial.print("\t ");
////    Serial.println(spdt_2);
////
////    Serial.println("\nPWM_3");
////    Serial.println(pwm_3);
////
////    //fan status
////    Serial.println("\nFans \t1 \t2");
////    Serial.print("\t");
////    Serial.print(fan_1);
////    Serial.print("\t");
////    Serial.print(fan_2);
//
//    // MCP4728s data
//    Serial.println("\nMCP4728");
//    Serial.println("0x \tDAC_A\tDAC_B\tDAC_C\tDAC_D");
//    Serial.print(MCP4728, HEX);
//    for(int i=0; i<=3; i++)
//    {
//      Serial.print("\t");
//      Serial.print(DACS[i],DEC);
//    }
//    Serial.print("\n");
//    Serial.print(MCP4728_P, HEX);
//    Serial.print("\t");
//    Serial.print(DAC_ref + DAC_P_led);
//    Serial.print("\n");
//    
//    
//    // MCP9904s data
//    Serial.println("\nMCP9904");
//    Serial.println("0x \tT_int \tT1 \tT2 \tT3");
//    for(int i=0; i<MCP9904_cnt; i++)
//    { 
//      Serial.print(MCP9904s[i],HEX);
//      Serial.print("\t");
//      Serial.print(Ts[i][0],1);
//      Serial.print("\t");
//      Serial.print(Ts[i][1],1);
//      Serial.print("\t");
//      Serial.print(Ts[i][2],1);
//      Serial.print("\t");
//      Serial.println(Ts[i][3],1);
//    }
//
//    // PID values ?
//
//    Serial.println("########");
//  }

//End of Serial print
  //Both LED are OFF 
  digitalWrite(LED_O, LOW);            
  digitalWrite(LED_B, LOW);
  
//Wait for end of loop target time
  if((millis()-t_0)>D){
    Serial.print("\t!Loop taget time Overflow! ");
    Serial.println((millis()-t_0),DEC);}
  while( (millis()-t_0)<D){delayMicroseconds(100);}
}
