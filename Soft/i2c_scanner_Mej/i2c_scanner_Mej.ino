    // --------------------------------------
    // i2c_scanner
    //
    // Le 8 Decembre 2018 by Mej
    // Copy/Pasted/Tweeked from https://playground.arduino.cc/Main/I2cScanner
     
#include <Wire.h>
const byte En_V3 = A2;  //pin38 PF5-ADC5 


void setup()
{
  pinMode(En_V3, OUTPUT);
  digitalWrite(En_V3, HIGH);
  delay(100);
  
  Wire.begin();
 
  Serial.begin(115200);
  while (!Serial);             // wait for serial monitor
  Serial.println("\nI2C Scanner");

  
  pinMode(A3, OUTPUT); //pin used to trig LDAC on Analog Discovery
  digitalWrite(A3, LOW);
  pinMode(7, INPUT); //pin1 Latch of MCP4728
}
 
 
void loop()
{
  byte error, address;
  int nDevices;
 
  Serial.println("Scanning...");
 
  nDevices = 0;
  for(address = 1; address < 127; address++ )
  {
    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    delayMicroseconds(100);
    Wire.beginTransmission(address);
    error = Wire.endTransmission();
 
    if (error == 0)
    {
      Serial.print("I2C device found at address 0x");
      if (address<16)
        Serial.print("0");
      Serial.print(address,HEX);
      Serial.print(" = ");
      Serial.print(address,BIN);
      Serial.print(" = ");
      Serial.println(address,DEC);
 
      nDevices++;
    }
    else if (error==4)
    {
      Serial.print("Unknown error at address 0x");
      if (address<16)
        Serial.print("0");
      Serial.print(address,HEX);
      Serial.print(" = ");
      Serial.print(address,BIN);
      Serial.print(" = ");
      Serial.println(address,DEC);
    }    
  }
  if (nDevices == 0)
    Serial.println("No I2C devices found\n");
  else
    Serial.println("done\n");

  
  Serial.println("Change MCP4728 address 000->011"); //following datasheet fig 5-11 p42/69
  delayMicroseconds(100);
  digitalWrite(A3, HIGH);
  Wire.beginTransmission(B1100000); // 000 Address
  Wire.write(B01100001); //2nd byte "LDAC should go down at thue end of this byte
  Wire.write(B01101110); //3rd byte
  Wire.write(B01101111); //4th Byte
  Wire.endTransmission();
  delayMicroseconds(100);
  digitalWrite(A3, LOW);
  
  
  Serial.print("Re-starting in ");
  for (int i=0; i<=5; i++){ // wait 5 seconds for next scan
    Serial.print(5-i);
    delay(100);
  }
  Serial.print("\n");
}

