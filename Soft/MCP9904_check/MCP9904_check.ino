// --------------------------------------
// MCP9804 (I2C temp sensor) test
//
// June 2018 by Mej
// 
     
/////////////////////////////////////////////////////////////////////////////////////////////////////////

// timing monitoring variables

unsigned long t_0 = millis(); // gives time since startup in ms. Overflow after 50 days.
unsigned long t = millis();
unsigned long loop_cnt = 0; //loop count
const int D = 1000; //delay between loops in ms


// Wire and I²C functions

#include <Wire.h>

void set_I2C_register(byte ADDRESS, byte REGISTER, byte VALUE)
{
  Wire.beginTransmission(ADDRESS);
  Wire.write(REGISTER);
  Wire.write(VALUE);
  Wire.endTransmission();
}

byte get_I2C_register(byte ADDRESS, byte REGISTER)
{
  Wire.beginTransmission(ADDRESS);
  Wire.write(REGISTER);
  Wire.endTransmission();
  Wire.requestFrom(ADDRESS, 1); //read 1 byte
  byte x = Wire.read();
  return x;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
//## MCP9904 (I²C thermometer) address
//Address
const byte MCP9904_10k  =  B1001100; // 4C
const byte MCP9904_22k = B0011100; // 1C    22kOhm
const byte MCP9904_33k = B0111100; // 3C    33kOhm or no resistance
//const byte MCP9904s[] = {MCP9904_1, MCP9904_22k, MCP9904_33k};
const byte MCP9904s[] = {MCP9904_10k};
const byte MCP9904_cnt =1;
byte MCP9904 = MCP9904_33k;
//reg values
const byte MCP9904_config = B00100000; //LED Alert ON - Device active - Comparator mode Alert -  Resistance Error Correction enabled - range 0 to 128°C - Averaging enabled - AntiParallel diode enabled
const int MCP9904_high_limit=128; //signed int between 0 and 128
const int MCP9904_low_limit =0;
//store the temperature of respectivelly: Internal, Q1, Q2, Q3
byte data1, data2; //Used to store reading from MCP9904 registers
float Ts[] = {66.6, 66.6, 66.6, 66.6}; //Used to store the temperature of respectivelly: Internal, Q1, Q2, Q3

 
void setup()
{
  //Serial set up
  Serial.begin(115200);
  while (!Serial);             // wait for serial monitor to be monitoring ^^
  delay(1000);
  Serial.println("\nSerial is up at 115200 baud");

  //Set and scan I²C bus
  Wire.begin();
  Serial.println("\nI2C Scanning...");
  byte error, address;
  int nDevices =0;
  for(address = 16; address < 100; address++ )
  {
    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    // Copy/Pasted/Tweeked from https://playground.arduino.cc/Main/I2cScanner
    delay(1);
    Wire.beginTransmission(address);
    error = Wire.endTransmission();
 
    if (error == 0)
    {
      Serial.print("I2C device found at address 0x");
      if (address<16)
        Serial.print("0");
      Serial.print(address,HEX);
      Serial.println("  !");
 
      nDevices++;
    }
    else if (error==4)
    {
      Serial.print("Unknown error at address 0x");
      if (address<16)
        Serial.print("0");
      Serial.println(address,HEX);
    }    
  }
  if (nDevices == 0)
    Serial.println("No I2C devices found\n");
  else
    Serial.println("done\n");


  //Set config of MCP9804s, cf datasheet p19/51 (Register Description)
  for(int i=0; i<MCP9904_cnt; i++)
  {
    MCP9904 = MCP9904s[i];
    //temperature alert: HI LIMIT TEMP HI BYTE :REGISTER 5-10
    set_I2C_register(MCP9904, 0x07, MCP9904_high_limit); //int
    set_I2C_register(MCP9904, 0x0D, MCP9904_high_limit); //ext1 
    set_I2C_register(MCP9904, 0x15, MCP9904_high_limit); //ext2
    set_I2C_register(MCP9904, 0x2C, MCP9904_high_limit); //ext3
    //temperature alert: LO LIMIT TEMP HI BYTE :REGISTER 5-12
    set_I2C_register(MCP9904, 0x08, MCP9904_low_limit); 
    set_I2C_register(MCP9904, 0x0E, MCP9904_low_limit);
    set_I2C_register(MCP9904, 0x16, MCP9904_low_limit); 
    set_I2C_register(MCP9904, 0x2D, MCP9904_low_limit); 
    //configuration
    set_I2C_register(MCP9904, 0x03, MCP9904_config); 
    set_I2C_register(MCP9904, 0x09, MCP9904_config); //Config register also an 0x09 register?..
  }

  
}
 
 
void loop()
{
  for(int i=0; i<MCP9904_cnt; i++)
  { 
    
    loop_cnt = loop_cnt+1;
    t_0 = millis();

  
    MCP9904 = MCP9904s[i]; //select address
    //////////////////////////////////////////////////// read MCP9804 data, cf datasheet p19/51 (Register Description)
    
    /// Int temp: T0
    data1 = get_I2C_register(MCP9904, 0x00); //T_int high byte, signed int
    data2 = get_I2C_register(MCP9904, 0x29); //T_int low byte, 3 usefull bits only (MSB), steps of 0.125°C
    Ts[0] = data1 + data2/32*0.125;
    delay(1);
    
    /// Ext temp: T1
    data1 = get_I2C_register(MCP9904, 0x01); //T_1 high byte
    data2 = get_I2C_register(MCP9904, 0x10); //T_1 low byte
    Ts[1] = data1 + data2/32*0.125;
    delay(1);
    
    /// Ext temp: T2
    data1 = get_I2C_register(MCP9904, 0x23); //T_2 high byte
    data2 = get_I2C_register(MCP9904, 0x24); //T_2 low byte
    Ts[2] = data1 + data2/32*0.125;
    delay(1);
  
    /// Ext temp: T3
    data1 = get_I2C_register(MCP9904, 0x2A); //T_2 high byte
    data2 = get_I2C_register(MCP9904, 0x2B); //T_2 low byte
    Ts[3] = data1 + data2/32*0.125;
    delay(1);
   
    
  
    //////////////////////////////////////////////////// print MCP9904 data
    Serial.print("MCP9904 0x");
    Serial.print(MCP9904,HEX);
    Serial.print(" T[°C]=  ");
    Serial.print(Ts[0],1);
    Serial.print("  ");
    Serial.print(Ts[1],1);
    Serial.print(" ");
    Serial.print(Ts[2],1);
    Serial.print(" ");
    Serial.print(Ts[3],1);
    Serial.print("\t");
    
  }

  t = millis();
  Serial.print("t[s]= ");
  Serial.print(t/1000.0, 3);
  Serial.print("\t t_loop[ms]= ");
  Serial.println(t-t_0, DEC);


  delay(D);

}

