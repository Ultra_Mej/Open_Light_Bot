/*
 2018/10/18
 By Mej, for Impact Photonics

 Interface_PCB with ATMega32U4 bootloaded as Arduino Leonardo
*/


//Libs
/////////////////////////////////////////////////////////////////////

#include <Wire.h>
#include <PID_v1.h> //https://playground.arduino.cc/Code/PIDLibrary


//MCU pin-maping
/////////////////////////////////////////////////////////////////////

const byte LED_B = 8;  //pin28 PB4-ADC11         Led Blue (bottom of MCU)
const byte LED_O = A5;  //pin41 PF0-ADC0         Led Orange (right of MCU)

const byte PWM_1  = 5;  //pin31* PC6-OC3A
const byte SPDT_1 = 13; //pin32* PC7-OC4A
const byte PWM_2  = 9;  //pin29* PB5-OC1A-ADC12
const byte SPDT_2 = 10; //pin30* PB3-OC1B-ADC13
const byte PWM_3  = 6;  //pin27* PD7-OC4D-ADC10-T0
const byte PWM_4  = 11; //pin12* PB7-OC0A

const byte DIR_1 = A4;  //pin40  PF1
const byte DIR_2 = A3;  //pin39  PF4-TCK-ADC4
const byte FAN_1 = A2;  //pin38  PF5
const byte FAN_2 = A1;  //pin37  PF6
const byte FAN_3 = A0;  //pin36  PF7
const byte TRIG_1 = 1;  //pin21 PD3-TX
const byte TRIG_2 = 4;  //pin25 PD4-ICP1-ADC8

const byte INT_1 = 1;     //pin20 PD2-RX            /INT of TCA9534 (left side)
const byte LDAC_1 = 7;    //pin1  PE6-AIN0-Int6     Latch of MCP4728

//const byte x =   12; //pin26                Not connected

//pin values
bool dir_1 = 0;
bool dir_2 = 0;
bool fan_1 = 0;
bool fan_2 = 0;
bool fan_3 = 0;
bool trig_1 = 0;
bool trig_2 = 0;
bool ldac_1 = 0;
bool int_1 = 0;

byte pwm_1 = 0;
byte pwm_2 = 0;
byte pwm_3 = 0;
byte pwm_4 = 0;
byte spdt_1 = 0;
byte spdt_2 = 0;


//timing monitoring
/////////////////////////////////////////////////////////////////////
unsigned long t_0 = millis(); // gives time since startup in ms. Overflow after 50 days.
unsigned long t = millis();
unsigned long loop_cnt = 0; //loop count
const int D = 100; //loop target duration in ms


//Wire and I²C functions
/////////////////////////////////////////////////////////////////////

byte data, data0, data1, data2; // used to buffer I²C data (in and/or out)

void set_I2C_register(byte ADDRESS, byte REGISTER, byte VALUE)
{
  Wire.beginTransmission(ADDRESS);
  delayMicroseconds(100);
  Wire.write(REGISTER);
  Wire.write(VALUE);
  Wire.endTransmission();
}

byte get_I2C_register(byte ADDRESS, byte REGISTER)
{
  Wire.beginTransmission(ADDRESS);
  delayMicroseconds(100);
  Wire.write(REGISTER);
  Wire.endTransmission();
  Wire.requestFrom(ADDRESS, 1); //read 1 byte
  byte x = Wire.read();
  return x;
}


//## MCP9904 (I²C thermometer, 1+3 channels)
  //Address
const byte MCP9904_0 = B0011100; // 1C  R=22k  LED_IMS
const byte MCP9904_1 = B1111100; // 7C  R=4.7k Interface_PCB, left side
const byte MCP9904_2 = B1011100; // 5C  R=6.8k Interface_PCB, right side
const byte MCP9904s[] = {MCP9904_0, MCP9904_1, MCP9904_2};
const byte MCP9904_cnt =3;
byte MCP9904 = MCP9904s[0];
  //reg
const byte MCP9904_config = B00100000; //LED Alert ON - Device active - Comparator mode Alert -  Resistance Error Correction enabled - range 0 to 128°C - Averaging enabled - AntiParallel diode enabled
  //store the temperature of respectivelly: Internal, Q1, Q2, Q3
float Ts[MCP9904_cnt][4] = { {66.6, 66.6, 66.6, 66.6}, //Used to store temps in [°C]
                             {66.6, 66.6, 66.6, 66.6},
                             {66.6, 66.6, 66.6, 66.6} };


//## MCP4728 (Quad DAC 12bit - Vref=2.048V)
//Address
const uint8_t MCP4728 = B1100011; //0x63  ->Udrive
const uint8_t MCP4728_P = B1100000; //0x60  ->Peltier
//mode
const byte MCP4728_MODE = B10000000;  // Vref interne - PD=PD0=G=0
//Commands (see from p31 of datasheet)
const byte MCP4728_RESET = 0x06;
const byte MCP4728_WAKEUP = 0x09;
const byte MCP4728_UPDATE = 0x08;
const byte MCP4728_ADDRESS = 0x0C; //can be changed in EEPROM!
const byte MCP4728_HARDW = B01010000; //Write all DAC imputs registers & EEPROM from Channel A to Channel D, see figure 5-9 p40
//const byte MCP4728_SOFTW = B00010000; //Write all DAC imputs registers from Channel A to Channel D
//DAC value entre 0 et 4095 (12bit) où 1LSB = 2.048/2**16=0.5mV       2048=1024mV    512= 256mV
//R sense = 2 Ohm, I_max=350mA => Ud_max=700mV => DAC_max=1400 (effective DAC resolution = ln2(1400) = 10.5bit )
unsigned int DACS[] = {1400, 700, 350, 175}; // Udrive: !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!TBD High_B, Low_B, Low_A, High_A
unsigned int DAC_ref = 3000;
unsigned int DAC_P = 0;

////## PCA9555 (I²C GPIO expender, 2x8bit)
//  //Address
//const byte PCA9555 =  B0100001; //  0x21  
//  //DRV8432 Input=1 / Output=0            //        MSB   6     5   4     3     2   1     LSB
//const byte PCA_port_0_config = B01100000; //port 0: LED   /OTW  /F  /RAB  M3    M2  M1    /RCD
//const byte PCA_port_0_reset =  B00000000;
//const byte PCA_port_0_init =   B10010001;
//  //DRV8412
//const byte PCA_port_1_config = B00000110; //port 1: /RCD  M1    M2  M3    /RAB  /F  /OTW  LED
//const byte PCA_port_1_reset =  B00000000;
//const byte PCA_port_1_init =   B10001001;
//  //Used to read PCA port
////port_0 = B11111111; //DRV8432 /F=bit(5)  /OTW=bit(6) 
////port_1 = B11111111; //DRV8412 /F=bit(2)  /OTW=bit(1)
//byte PCA_0; //Used to read port0, connected to DRV8432 /F=bit(5)  /OTW=bit(6) 
//byte PCA_1; //Used to read port1, connected to DRV8412 /F=bit(2)  /OTW=bit(1)

//## PID
  //Define Variables we'll be connecting to
double Setpoint_LED, Input_LED, Output_LED; //LED PCB, petit peltier connected to DRV8412_AB
double Setpoint_CUV, Input_CUV, Output_CUV; //CUV Holder, petit peltier connected to DRV8412_CD
double Setpoint_P_1, Input_P_1, Output_P_1; //Plate 1, big peltier connected to DRV8432_AB
double Setpoint_P_2, Input_P_2, Output_P_2; //Plate 2, big peltier connected to DRV8432_CD
   //Specify the links and initial tuning parameters
   // for tunningn cf: https://en.wikipedia.org/wiki/Ziegle-Nichols_method
PID myPID_LED(&Input_LED, &Output_LED, &Setpoint_LED, 10, 2, 2, REVERSE); //
PID myPID_CUV(&Input_CUV, &Output_CUV, &Setpoint_CUV, 20, 0.4, 0.4, REVERSE); // Ku entre 100 et 200. @150, Tu=30s => Kp=90, Ki=15, Kd=3.75;
PID myPID_P_1(&Input_P_1, &Output_P_1, &Setpoint_P_1, 20, 0.4, 0.4, REVERSE);
PID myPID_P_2(&Input_P_2, &Output_P_2, &Setpoint_P_2, 20, 0.4, 0.4, REVERSE);
  //Target temps
float T_CUV_target = 35;
float T_LED_target = 15;
float T_P_1_target = 25;
float T_P_2_target = 25;



/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
//  SETUP
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

void setup() {

  //MCU pin set up
  /////////////////////////////////////////////////////////////////////

  pinMode(LED_B, OUTPUT);
  pinMode(LED_O, OUTPUT);
  
  pinMode(DIR_1, OUTPUT);
  pinMode(DIR_2, OUTPUT);
  pinMode(FAN_1, OUTPUT);
  pinMode(FAN_2, OUTPUT);
  pinMode(FAN_3, OUTPUT);

  pinMode(SPDT_1, OUTPUT);
  pinMode(SPDT_2, OUTPUT);
  pinMode(PWM_1, OUTPUT);
  pinMode(PWM_2, OUTPUT);
  pinMode(PWM_3, OUTPUT);
  pinMode(PWM_4, OUTPUT);
  
  pinMode(TRIG_1, OUTPUT);
  pinMode(TRIG_2, INPUT);

  pinMode(LDAC_1, OUTPUT);
  pinMode(INT_1, INPUT);
  
  
  //Write pin idle
  
  digitalWrite(LED_B, LOW);
  digitalWrite(LED_O, LOW);
  digitalWrite(DIR_1, dir_1);
  digitalWrite(DIR_2, dir_2);
  digitalWrite(FAN_1, fan_1);
  digitalWrite(FAN_2, fan_2);
  digitalWrite(FAN_3, fan_3);
  digitalWrite(TRIG_1, trig_1);
  digitalWrite(LDAC_1, ldac_1);

  analogWrite(PWM_1, pwm_1); 
  analogWrite(PWM_2, pwm_2); 
  analogWrite(PWM_3, pwm_3);
  analogWrite(PWM_4, pwm_4);
  analogWrite(SPDT_1, spdt_1); 
  analogWrite(SPDT_2, spdt_2);

  trig_2 = digitalRead(TRIG_2);
  int_1 = digitalRead(INT_1);


  //MCU blink
  for(int i=0;i<5;i++)
  {
    digitalWrite(LED_B, HIGH);
    digitalWrite(LED_O, LOW);
    delay(50);
    digitalWrite(LED_B, LOW);
    digitalWrite(LED_O, HIGH);
    delay(50);
  }
  digitalWrite(LED_B, LOW);
  digitalWrite(LED_O, LOW);


  //Serial coms set-up
  Serial.begin(115200); // in BAUD = 14.4kB/s
  delay(200);
  while (!Serial);             // wait for serial monitor to be monitoring ^^
  Serial.println("\n\n\n############################################################");
  Serial.println("#!  ATMega32U4 bootlaoded as Arduino Leonardo is awake     #");
  Serial.println("############################################################");
  Serial.println("\nSerial is up at 115200 baud");
  delay(200);
  
  //MCU PWM set up on highest frequency (62.5kHz <=> no prescaling), mode "fast pwm 8 bit" (ie mode 5, see p133/438 of datasheet)
  //notes on timer regs: https://www.locoduino.org/spip.php?article89
  Serial.println("\nTimers set for 'fast pwm'");
  byte Timer_mode = 5; // "fast pwm" (ie single slope) , 8 bit
  bool WGM_0 = bitRead(Timer_mode,0);
  bool WGM_1 = bitRead(Timer_mode,1);
  bool WGM_2 = bitRead(Timer_mode,2);
  bool WGM_3 = bitRead(Timer_mode,3);
  
  //PWM_2 on pin29* PB5 - OC1A - Timer 1 (reg TCCR1X) compare register OCR1A
  //SPDT_2 on pin30* PB3 - OC1B - Timer 1 (reg TCCR1X) compare register OCR1B
  TCCR1A = TCCR1A/4*4 + (WGM_1*2+WGM_0);
  TCCR1B = TCCR1A/32*32 + (WGM_3*16+WGM_2*8+B001); //No prescaling
  Serial.println("\tTIMER 1(PWM_2, SPDT_2) set at 62kHz");
  
  //PWM_1 on pin31* PC6-OC3A - Timer 3 (reg TCCR3X) compare register OCR3A
  TCCR3A = TCCR3A/4*4 + (WGM_1*2+WGM_0);
  TCCR3B = TCCR3A/32*32 + (WGM_3*16+WGM_2*8+B001); //No prescaling
  Serial.println("\tTIMER 3(PWM_1) set at 62kHz");
  
  //SPDT_1 on pin32*
  //PWM_3 on pin27* PD7-OC4D - Timer 4 (reg TCCR4X) compare register OCR4D   !NB! modify PLL register "PLLFRQ" for even faster PWM
  TCCR4B = TCCR4B/16*16 + B0001;
  TCCR4B = TCCR4B|B10000000;
  TCCR4D = TCCR4D/4*4 + B00;
  Serial.println("\tTIMER 4(SPDT_1, PWM_3) set at 62kHz");

  //Changing freq of Timer0 <=> PWM_4 (pin11 would change the I2C frequency?..)
  Serial.println("\tTIMER 0(PWM_4) un-alterded");

  //Set and scan I²C bus  
  Wire.begin();
  Serial.println("\nI2C Scanning...");
  byte error, address;
  int nDevices =0;
  for(address = 16; address < 100; address++ )
  {
    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    // Copy/Pasted/Tweeked from https://playground.arduino.cc/Main/I2cScanner
    delay(10);
    Wire.beginTransmission(address);
    error = Wire.endTransmission();
    if (error == 0)
    {
      Serial.print("I2C device found at address 0x");
      if (address<16)
        Serial.print("0");
      Serial.print(address,HEX);
      Serial.println("  !");
 
      nDevices++;
    }
    else if (error==4)
    {
      Serial.print("Unknown error at address 0x");
      if (address<16)
        Serial.print("0");
      Serial.println(address,HEX);
    }    
  }
  if (nDevices == 0)
    Serial.println("No I2C devices found\n");
  else
    Serial.print("done\n");
  delay(100);


////Set PCA9555
//  //blink
//  set_I2C_register(PCA9555, 0x06, PCA_port_0_config); //Config port 0
//  set_I2C_register(PCA9555, 0x07, PCA_port_1_config); //Config port 1
//  delay(100);
//  set_I2C_register(PCA9555, 0x02, PCA_port_0_reset); //Output port 0
//  set_I2C_register(PCA9555, 0x03, PCA_port_1_reset); //Config port 1
//  delay(100);
//  for(int i=0; i<3; i++)
//  {
//    set_I2C_register(PCA9555, 0x02, PCA_port_0_init); //Output port 0
//    set_I2C_register(PCA9555, 0x03, PCA_port_1_init); //Config port 1
//    delay(100);
//    set_I2C_register(PCA9555, 0x02, PCA_port_0_reset); //Output port 0
//    set_I2C_register(PCA9555, 0x03, PCA_port_1_reset); //Config port 1
//    delay(100);
//  }
//  //ON/OFF
////  set_I2C_register(PCA9555, 0x02, PCA_port_0_reset); //Output port 0, control DRV8432
////  set_I2C_register(PCA9555, 0x03, PCA_port_1_init); //Config port 1, control DRV8412
//  //Read and print pin states
//  PCA_0 = get_I2C_register(PCA9555, 0x00); //Output port 0
//  PCA_1 = get_I2C_register(PCA9555, 0x01); //Output port 1
//  Serial.println("\nPCA9555 is set-up");
//  Serial.print("\tPort0=\t");
//  Serial.println(PCA_0,BIN);
//  Serial.print("\tPort1=\t");
//  Serial.println(PCA_1,BIN);


//Set MCP9904s, see datasheet p19/51 for register description
  for(int i=0; i<MCP9904_cnt; i++)
  {
    MCP9904 = MCP9904s[i];
    set_I2C_register(MCP9904, 0x03, MCP9904_config); 
    set_I2C_register(MCP9904, 0x09, MCP9904_config); //Config register also an 0x09 register?..
  }
  Serial.println("\nMCP9904s are set-up");


  //Set MCP4728
  delay(1);
  Serial.println(" ");
  Serial.print("MCP4728 is RESET, ");
  Wire.beginTransmission(MCP4728);
  Wire.write(MCP4728_RESET);
  Wire.endTransmission();
  delay(100);
  Serial.print("loaded with [");
  Wire.beginTransmission(MCP4728);
  Wire.write(MCP4728_HARDW);
  for(int i=0; i<4; i++){
    Serial.print(DACS[i]/2/2); //0.5mV per value, 2 Ohm sensing
    Serial.print(" ");
    byte Byte1 = (DACS[i]>>8); //first bits (12-8=4bits)
    Byte1 = MCP4728_MODE|Byte1;
    byte Byte2 = DACS[i]&0xff; //last byte
    Wire.write(Byte1);
    Wire.write(Byte2);
    /*
    Serial.print("=");
    Serial.print(Byte1,BIN);
    Serial.print(" ");
    Serial.print(Byte2,BIN);
    */
    }
  Wire.endTransmission();
  Serial.println("] mA and is ready to go");


  //Set MCP4728_P
  delay(1);
  Serial.println(" ");
  Serial.print("MCP4728_P is RESET, ");
  Wire.beginTransmission(MCP4728_P);
  Wire.write(MCP4728_RESET);
  Wire.endTransmission();
  delay(100);
  Serial.print("loaded with [");
  Wire.beginTransmission(MCP4728_P);
  Wire.write(MCP4728_HARDW);
  for(int i=0; i<4; i++){
    Serial.print(DAC_ref/2); //0.5mV per value, =>1500mV=1.5V=Vref MAX1968
    Serial.print(" ");
    byte Byte1 = (DAC_ref>>8); //first bits (12-8=4bits)
    Byte1 = MCP4728_MODE|Byte1;
    byte Byte2 = DAC_ref&0xff; //last byte
    Wire.write(Byte1);
    Wire.write(Byte2);
    }
  Wire.endTransmission();
  Serial.println("] mV and is ready to go");



//Set PIDs
//  Input_LED = (TTs[0]+TTs[1])/2; //initialize the variables we're linked to
//  Setpoint_LED = T_LED_target;
//  Output_LED = 128;
//  myPID_LED.SetMode(AUTOMATIC); //turn the PID on
//  myPID_LED.SetSampleTime(200);
//  
//  Input_CUV = TTs[2]; //initialize the variables we're linked to
//  Setpoint_CUV = T_CUV_target;
//  Output_CUV = 128;
//  myPID_CUV.SetMode(AUTOMATIC); //turn the PID on
//  myPID_CUV.SetSampleTime(200);
//  
//  Input_P_1 = Ts[0][2]; //initialize the variables we're linked to
//  Setpoint_P_1 = T_P_1_target;
//  Output_P_1 = 128;
//  myPID_P_1.SetMode(AUTOMATIC); //turn the PID on
//  myPID_P_1.SetSampleTime(200);
//  
//  Input_P_2 = Ts[3][2]; //initialize the variables we're linked to
//  Setpoint_P_2 = T_P_2_target;
//  Output_P_2 = 128;
//  myPID_P_2.SetMode(AUTOMATIC); //turn the PID on
//  myPID_P_2.SetSampleTime(200);
  
  Serial.println("\nPIDs are NOT set-up");


  Serial.println("\n\n");
}



/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
// LOOP
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

void loop() {

//Start of usefull loop
  loop_cnt = loop_cnt+1;
  t_0 = millis();
  //LED Orange is ON alone
  digitalWrite(LED_B, LOW);
  digitalWrite(LED_O, HIGH);
  digitalWrite(TRIG_1, HIGH);
  

//// Read input pin
  int_1 = digitalRead(INT_1);  
  
//// Read PCA
//  PCA_0 = get_I2C_register(PCA9555, 0x00);
//  PCA_1 = get_I2C_register(PCA9555, 0x01);


// Read MCP99004s
  for(int i=0; i<MCP9904_cnt; i++)
  {  
    MCP9904 = MCP9904s[i]; //select address
    
    data1 = get_I2C_register(MCP9904, 0x00); //T_int high byte, signed int
    data2 = get_I2C_register(MCP9904, 0x29); //T_int low byte, 3 usefull bits only (MSB), steps of 0.125°C
    Ts[i][0] = data1 + data2/32*0.125;
    
    data1 = get_I2C_register(MCP9904, 0x01); //T_1 high byte
    data2 = get_I2C_register(MCP9904, 0x10); //T_1 low byte
    Ts[i][1] = data1 + data2/32*0.125;

    data1 = get_I2C_register(MCP9904, 0x23); //T_2 high byte
    data2 = get_I2C_register(MCP9904, 0x24); //T_2 low byte
    Ts[i][2] = data1 + data2/32*0.125;

    data1 = get_I2C_register(MCP9904, 0x2A); //T_2 high byte
    data2 = get_I2C_register(MCP9904, 0x2B); //T_2 low byte
    Ts[i][3] = data1 + data2/32*0.125;
  }


//Update T_PID_target ??


// PID Algorithme
//  //LED PID
//  Input_LED = (TTs[0]+TTs[1])/2;
//  Setpoint_LED = T_LED_target;
//  myPID_LED.Compute();
//  pwm_a = Output_LED/255*2400; //! Output varies between 0 and 255 !
//  io_a = 
//  io
//  //CUV PID
//  Input_CUV = TTs[2];
//  Setpoint_CUV = T_CUV_target;
//  myPID_CUV.Compute();
//  pwm_b = Output_CUV/255*2400; //! Output varies between 0 and 255 !
//  io_b = 
//  //P_1 PID
//  Input_P_1 = Ts[0][2];
//  Setpoint_P_1 = T_P_1_target;
//  myPID_P_1.Compute();
//  pwm_c = Output_P_1/255*2400; //! Output varies between 0 and 255 !
//  io_c = 
//  //P_2 PID
//  Input_P_2 = Ts[2][2];
//  Setpoint_P_2 = T_P_2_target;
//  myPID_P_2.Compute();
//  pwm_d = Output_P_2/255*2400; //! Output varies between 0 and 255 !
//  io_d =

  DAC_P = 1000;

  //update pins value
  dir_1 = !dir_1;
  dir_2 = !dir_2;
  fan_1 = !fan_1;
  fan_2 = !fan_2;
  fan_3 = !fan_3;
  //trig_1 = !trig_1;

  //pwm_1 = loop_cnt%256;
  //pwm_2 = loop_cnt%256;
  pwm_3 = loop_cnt%256;
  pwm_4 = loop_cnt%256;
  spdt_1 = loop_cnt%256;
  spdt_2 = loop_cnt%256;

  pwm_1 = 255;
  pwm_2 = 255;
  



  //writte MCP4728
  Wire.beginTransmission(MCP4728);
  Wire.write(MCP4728_HARDW);
  for(int i=0; i<=3; i++){
    byte Byte1 = MCP4728_MODE|(DACS[i]>>8); //first bits (12-8=4bits) + config
    Wire.write(Byte1);
    byte Byte2 = DACS[i]&0xff; //last byte
    Wire.write(Byte2);
    }
  Wire.endTransmission();

  //writte MCP4728_P
  Wire.beginTransmission(MCP4728_P);
  Wire.write(MCP4728_HARDW);
  unsigned int value = DAC_ref + DAC_P;
  for(int i=0; i<=3; i++){
    byte Byte1 = MCP4728_MODE|(value>>8); //first bits (12-8=4bits) + config
    Wire.write(Byte1);
    byte Byte2 = value&0xff; //last byte
    Wire.write(Byte2);
    }
  Wire.endTransmission();


//Write MCU pins
  digitalWrite(DIR_1, dir_1);
  digitalWrite(DIR_2, dir_2);
  digitalWrite(FAN_1, fan_1);
  digitalWrite(FAN_2, fan_2);
  digitalWrite(FAN_3, fan_3);
  digitalWrite(TRIG_1, trig_1);
  digitalWrite(LDAC_1, ldac_1);

  analogWrite(PWM_1, pwm_1); 
  analogWrite(PWM_2, pwm_2); 
  analogWrite(PWM_3, pwm_3);
  analogWrite(PWM_4, pwm_4);
  analogWrite(SPDT_1, spdt_1); 
  analogWrite(SPDT_2, spdt_2);

//// Re-write PCA ON/OFF
//  set_I2C_register(PCA9555, 0x02, PCA_port_0_reset); //Output port 0, control DRV8432
//  set_I2C_register(PCA9555, 0x03, PCA_port_1_reset); //Config port 1, control DRV8412

  
//End of usefull loop
  t = millis();
  //LED Blue is ON alone
  digitalWrite(LED_B, HIGH);
  digitalWrite(LED_O, LOW);
  digitalWrite(TRIG_1, LOW);
 

//Serial Print (minimal)
  Serial.println("########");
  Serial.print(loop_cnt,DEC);
  //Loop timing
  Serial.print("\tt_loop[ms]= ");
  Serial.print(t-t_0, DEC);
  Serial.print(" ");
  Serial.print("t[s]= ");
  Serial.println(t/1000.0, 3);
  

//Serial Print (full data)
  if(loop_cnt%1==0)
  {
   //pin out status
    Serial.println("\nA3\tA4");
    Serial.print(dir_1);
    Serial.print("\t");
    Serial.println(dir_2);
  
    //fan status
    Serial.println("\nFans \t0 \t1 \t2");
    Serial.print("\t");
    Serial.print(fan_1);
    Serial.print("\t");
    Serial.print(fan_2);
    Serial.print("\t");
    Serial.println(fan_3);

    //PWMs status
    Serial.println("\nPWM_1 \t SPDT_1");
    Serial.print(pwm_1);
    Serial.print("\t ");
    Serial.println(spdt_1);

    Serial.println("\nPWM_2 \t SPDT_2");
    Serial.print(pwm_2);
    Serial.print("\t ");
    Serial.println(spdt_2);

    Serial.println("\nPWM_3 \t PWM_4");
    Serial.print(pwm_3);
    Serial.print("\t ");
    Serial.println(pwm_4);

    // MCP4728s data
    Serial.println("\nMCP4728");
    Serial.println("0x \tDAC_A\tDAC_B\tDAC_C\tDAC_D");
    Serial.print(MCP4728, HEX);
    for(int i=0; i<=3; i++)
    {
      Serial.print("\t");
      Serial.print(DACS[i],DEC);
    }
    Serial.print("\n");
    Serial.print(MCP4728_P, HEX);
    Serial.print("\t");
    Serial.print(value);
    Serial.print("\n");
    
    
    // MCP9904s data
    Serial.println("\nMCP9904");
    Serial.println("0x \tT_int \tT1 \tT2 \tT3");
    for(int i=0; i<MCP9904_cnt; i++)
    { 
      Serial.print(MCP9904s[i],HEX);
      Serial.print("\t");
      Serial.print(Ts[i][0],1);
      Serial.print("\t");
      Serial.print(Ts[i][1],1);
      Serial.print("\t");
      Serial.print(Ts[i][2],1);
      Serial.print("\t");
      Serial.println(Ts[i][3],1);
    }


    // PID values ?

    // MCU pins ?
  
    Serial.println("########");
  }

//End of Serial print
  //Both LED are OFF 
  digitalWrite(LED_O, LOW);            
  digitalWrite(LED_B, LOW);
  
//Wait for end of loop target time
  if((millis()-t_0)>D){
    Serial.print("\t!Loop taget time Overflow! ");
    Serial.println((millis()-t_0),DEC);}
  while( (millis()-t_0)<D){
    delayMicroseconds(10);}
}
